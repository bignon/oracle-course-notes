up:
	@docker run -d --name oracle \
  	--privileged -v $(pwd)/oradata:/u01/app/oracle \
  	-p 8080:8080 -p 1521:1521 absolutapps/oracle-12c-ee;

down:
	@docker container rm oracle || true

start:
	@docker container start oracle

stop:
	@docker container stop oracle || true

bash:
	@docker exec -it oracle bash

sql:
	@docker exec -it oracle sqlplus /nolog

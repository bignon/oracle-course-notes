# Notes pour le cours d'ORACLE
Ce depot est destine a contenir des notes et des instructions concernant l'usage d'ORACLE
dans le cadre du cours.

La version d'ORACLE utilisee est la 12c.

## Installation
- Windows  
Ceux sous Windows peuvent suivre la procedure classique en executant l'installer Oracle.
- Linux  
Vous pouvez utiliser le fichier `Makefile` cree dans ce depot via `make` ou en copiant les commandes depuis le fichier.
Pour demarrer, par exemple:
```bash
make up
```
ou  
```bash
docker container inspect oracle && \
		docker container start oracle >/dev/null 2>&1 || docker run -d --name oracle \
  	--privileged -v $(pwd)/oradata:/u01/app/oracle \
  	-p 8080:8080 -p 1521:1521 absolutapps/oracle-12c-ee;
```

Consulter le fichier pour le reste des commandes.

Sorry pour les accents, QWERTY for life :) !

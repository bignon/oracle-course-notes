#!/bin/bash


if [ $(docker inspect oracle) -ne 0 ]
then
  echo "Creating a new ORACLE container...";

  docker run -d --name oracle \
  --privileged -v $(pwd)/oradata:/u01/app/oracle \
  -p 8080:8080 -p 1521:1521 absolutapps/oracle-12c-ee;

  echo "Created."
else
  echo "Starting existing ORACLE container...";
  docker container start oracle;
  echo "Started."
fi

